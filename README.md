# LOLStatAnalyst
*Feng Zhang*, *J Wang*, *Shuihui Wu* and *Thi Tran Nguyen*

Revised Nov. 11 2018

Version: 0.1

<img src="https://i.redd.it/nlgi3f7qfty11.jpg" width=25%>

## Project Description
This project is about extracting *Leage of Legend (LOL)* gaming characteristics 
and winning patterns by analyzing LOL datasets using MapReduce.

## Objectives

- [ ] Programming

    - [ ] Mapper
    - [ ] Reducer
    - [ ] Test run
    
- [ ] Analyzing

    - [ ] Analyzing Skills of the players

        - Wining Ratio
        - "Activeness" of the players (moving distance, alive time, skill used)
        - Most played time player
        - Most winning player
        - etc.
    
    - [ ] Linguistic Analysis

        - Most frequent word used by toxic/normal players
        - etc.
    
    - [ ] Winning Patterns (analysis in discrete time)

        - Ward position/number
        - Minion kills
        - Economics
        - Item Purchases
        - Buffs
    
    - [ ] Heat map of economics by location
    
## Project Structure
```
.
├── LoLStatAnalyst.iml
├── README.md (readme file)
├── lsa.iml
├── pom.xml (maven properties)
└── src
    └── main
        └── java (Sources Root)
            ├── jobs (concrete jobs put in here)
            │   └── LSAMain.java (default job)
            ├── mapper (concrete mapper put in here)
            │   └── DummyMapper.java
            ├── reducer (concrete reducer put in here)
            │   └── DummyReducer.java
            └── utils (utilities put in here, e.g. JSON Parser)
                └── DummyUtils.java

```

## Build & Run
Use command `mvn package` to create jar file with dependencies.

*In odrder to separate different version of jobs when running a job using
 MapReduce, we can now determine the **job name** using
 the following command:*
 ```commandline
hadoop jar jar_file main_class job_name output_dir
```
*Example:*
```commandline
hadoop jar lsa-0.1-jar-with-dependencies.jar jobs.LSAMain LOLStatTestRun output/10111007
```

package mapper;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.GameParser;

import java.io.IOException;

public class BufferAnalysisMapper extends Mapper<Object, Text, Text, Text> {
    private Text emitKey = new Text();
    private Text emitValue = new Text();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        GameParser parser = new GameParser(line);
        if (parser.getMapID() == 1) {
            String gameID = parser.getGame().getInt("game_id") + "";
            emitKey.set(gameID);
            emitValue.set(parser.getBuffInfoStr());
            context.write(emitKey, emitValue);
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }
}

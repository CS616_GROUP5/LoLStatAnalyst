package mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.GameParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class MinionsKillingMapper extends Mapper<Object, Text, Text, IntWritable> {


    private Text emitKey = new Text();
    private IntWritable emitValue = new IntWritable();



    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        GameParser parser = new GameParser(line);
        if (parser.getMapID() == 1) {
            HashMap<String, Integer> minionResult = parser.MinionKillerResult();
            ArrayList<String> minionPhaseArray = new ArrayList<String>(minionResult.keySet());
            for(String s : minionPhaseArray){
                emitKey.set(s + "\t" + parser.getRegion()); // Key: String, minionPhase + region
                emitValue.set(minionResult.get(s));   // Value: int
                context.write(emitKey, emitValue);
            }

        }


    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }


}

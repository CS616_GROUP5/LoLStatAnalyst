package mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.GameParser;

import java.io.IOException;

public class Top10ChampionMapper extends Mapper<Object, Text, Text, Text> {
    private final static IntWritable one = new IntWritable(1);
    private final static IntWritable zero = new IntWritable(0);

    private Text emitKey = new Text();
    private Text emitValue = new Text();


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        GameParser parser = new GameParser(line);
        if (parser.getMapID() == 1) {
            for (String id : parser.getChampionList()) {
                emitKey.set(id + "\t" + parser.getRegion()); // championID + region
                if (parser.isWinChampion(id)) {   // [ champion, (win \t lose) ]
                    emitValue.set(one + "\t" + zero);
                } else {
                    emitValue.set(zero + "\t" + one);
                }
            }
            context.write(emitKey, emitValue);
        }


    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }


}

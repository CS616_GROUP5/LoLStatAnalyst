package mapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.HateWordUtils;

import java.nio.charset.StandardCharsets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HateWordMapper extends Mapper<Object, Text, Text, IntWritable> {
    private Map<String, List<String>> dicmap = new HashMap<>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration config = context.getConfiguration();
        String dicPath = config.get("swearwords");
        try {
            Path path = new Path(dicPath);
            FileSystem fs = path.getFileSystem(config);
            InputStream in = fs.open(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            String line = br.readLine();

            while (line != null) {
                String word[] = line.split(",");
                String key = word[0];
                List<String> meanings = new ArrayList<>();
                for (int i = 1; i < word.length; i++)
                    meanings.add(word[i]);
                dicmap.put(key, meanings);   // constructing dictionary model using hashmap
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String content = HateWordUtils.getChatContent(value.toString());
        List<String> words = HateWordUtils.filterstopwords(content);
        int numOfChat = HateWordUtils.getChatNum(value.toString());
        context.write(new Text("ChatNum"), new IntWritable(numOfChat));
        for (String s : words)
            if (dicmap.containsKey(s))
                context.write(new Text(s), new IntWritable(1));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }
}

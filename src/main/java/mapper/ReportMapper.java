package mapper;

import com.alibaba.fastjson.JSON;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.beans.Tribunal;

import java.io.IOException;

public class ReportMapper extends Mapper<Object, Text, Text, IntWritable> {
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        Tribunal tribunal = JSON.parseObject(value.toString(), Tribunal.class);
        String report = tribunal.getMost_common_report_reason();

        context.write(new Text(report), new IntWritable(1));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }
}

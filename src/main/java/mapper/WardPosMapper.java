package mapper;

import com.alibaba.fastjson.JSON;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.WardPosUtils;
import utils.beans.LolObject;
import utils.beans.Ward;

import java.io.IOException;
import java.util.List;

public class WardPosMapper extends Mapper<Object, Text, Text, IntWritable> {
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String json = value.toString();
        LolObject jsonObj = JSON.parseObject(json, LolObject.class);
        List<Ward> wardInfo = jsonObj.getData().getWardFrames();
        double length = jsonObj.getLength();

        for (Ward ward : wardInfo){
            double time = ward.getStartTime();
            boolean iswinner = WardPosUtils.isWinner(jsonObj, ward);
            String coordinate = ward.getX() + " " + ward.getY();
            if (time >= 0 && time <= 600 && iswinner)  //initial phase
                context.write(new Text("win_ini_" + coordinate), new IntWritable(1));
            else if (time >= 0 && time <= 600 && !iswinner)
                context.write(new Text("los_ini_" + coordinate), new IntWritable(1));
            else if (time > 600 && time <= 1500 && iswinner)
                context.write(new Text("win_mid_" + coordinate), new IntWritable(1));
            else if (time > 600 && time <= 1500 && !iswinner)
                context.write(new Text("los_mid_" + coordinate), new IntWritable(1));
            else if (time > 1500 && time <= length && iswinner)
                context.write(new Text("win_end_" + coordinate), new IntWritable(1));
            else if (time > 1500 && time <= length && !iswinner)
                context.write(new Text("los_end_" + coordinate), new IntWritable(1));
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

}

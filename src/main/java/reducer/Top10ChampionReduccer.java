package reducer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class Top10ChampionReduccer extends Reducer<Text, Text, Text, Text> {

    private Text value = new Text();


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        int win = 0;
        int lose = 0;
        int sum = 0;
        double ratio = 0.0;

        for( Text t : values){  // [ champion, (win \t lose) ]
            String[] s = t.toString().split("\t");
            win += Integer.parseInt(s[0]);
            lose += Integer.parseInt(s[1]);

        }
        sum = win + lose;
        ratio = (win + 0.0)/sum;
        value.set(win + "\t" + lose + "\t" + sum +"\t" + ratio);
        context.write(key, value);

    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

}

package reducer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class PlayerAnalysisReducer
        extends Reducer<Text, Text, Text, Text> {
    private Text emitValue = new Text();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        int winCount = 0;
        int loseCount = 0;
        for (Text val : values) {
            String[] s = val.toString().split("\t");
            winCount += Integer.parseInt(s[0]);
            loseCount += Integer.parseInt(s[1]);
        }
        double ratio = (winCount + 0.0) / (winCount + loseCount);
        emitValue.set(winCount + "\t" + loseCount + "\t" + ratio);
        context.write(key, emitValue);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

}

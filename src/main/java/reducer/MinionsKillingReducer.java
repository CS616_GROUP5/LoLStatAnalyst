package reducer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MinionsKillingReducer extends Reducer<Text, IntWritable, Text, IntWritable>{


    @Override
    protected void setup(Reducer.Context context) throws IOException, InterruptedException {
        super.setup(context);
    }


    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

        int num = 0;

        for(IntWritable t: values){   // [ 1st_W na,  int ]
            num += t.get();
        }

        context.write(key, new IntWritable(num));

    }

    @Override
    protected void cleanup(Reducer.Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

}



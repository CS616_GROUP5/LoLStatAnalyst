package reducer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

public class WardPosReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    private MultipleOutputs<Text, IntWritable> mos;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        mos = new MultipleOutputs<Text, IntWritable>(context);
    }

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable i : values)
            sum += i.get();

        String[] keys = key.toString().split("_");
        if (keys[0].equals("win") && keys[1].equals("ini"))
            mos.write("WinIniPhase", new Text(keys[2]), new IntWritable(sum));
        else if (keys[0].equals("los") && keys[1].equals("ini"))
            mos.write("LosIniPhase", new Text(keys[2]), new IntWritable(sum));
        else if (keys[0].equals("win") && keys[1].equals("mid"))
            mos.write("WinMidPhase", new Text(keys[2]), new IntWritable(sum));
        else if (keys[0].equals("los") && keys[1].equals("mid"))
            mos.write("LosMidPhase", new Text(keys[2]), new IntWritable(sum));
        else if (keys[0].equals("win") && keys[1].equals("end"))
            mos.write("WinEndPhase", new Text(keys[2]), new IntWritable(sum));
        else if (keys[0].equals("los") && keys[1].equals("end"))
            mos.write("LosEndPhase", new Text(keys[2]), new IntWritable(sum));

    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
        mos.close();
    }
}

package jobs;

import mapper.PlayerAnalysisMapper;
import mapper.Top10ChampionMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import reducer.PlayerAnalysisReducer;
import reducer.Top10ChampionReduccer;

public class Top10Champion {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Top10Champion");
        job.setJarByClass(Top10Champion.class);
        job.setMapperClass(Top10ChampionMapper.class);
        job.setCombinerClass(Top10ChampionReduccer.class);
        job.setReducerClass(Top10ChampionReduccer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/riftwalk.gg"));
        FileOutputFormat.setOutputPath(job, new Path(args[0]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

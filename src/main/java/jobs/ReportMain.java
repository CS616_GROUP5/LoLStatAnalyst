package jobs;

import mapper.ReportMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import reducer.ReportReducer;

public class ReportMain {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "ReportReason");
        job.setJarByClass(ReportMain.class);
        job.setMapperClass(ReportMapper.class);
        job.setCombinerClass(ReportReducer.class);
        job.setReducerClass(ReportReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/lol-tribunal/na/na.jsons"));
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/lol-tribunal/euw/euw.jsons"));
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/lol-tribunal/kr/kr.jsons"));
        FileOutputFormat.setOutputPath(job, new Path(args[0]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

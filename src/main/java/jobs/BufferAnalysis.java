package jobs;

import mapper.BufferAnalysisMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import reducer.BufferAnalysisReducer;

public class BufferAnalysis {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "LOLStatAnalyst - BufferAnalysis");
        job.setJarByClass(BufferAnalysis.class);
        job.setMapperClass(BufferAnalysisMapper.class);
        job.setCombinerClass(BufferAnalysisReducer.class);
        job.setReducerClass(BufferAnalysisReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/riftwalk.gg"));
        FileOutputFormat.setOutputPath(job, new Path(args[0]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

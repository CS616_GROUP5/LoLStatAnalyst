package jobs;

import mapper.HateWordMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import reducer.HateWordReducer;

public class HateWordMain {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("swearwords", "/user/shuhui/vocabulary_trim.csv");

        Job job = Job.getInstance(conf,"HateWord");
        job.setJarByClass(HateWordMain.class);
        job.setMapperClass(HateWordMapper.class);
        job.setCombinerClass(HateWordReducer.class);
        job.setReducerClass(HateWordReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/lol-tribunal/na/na.jsons"));
        FileInputFormat.addInputPath(job, new Path("/user/blackburn/lol-tribunal/euw/euw.jsons"));
        FileOutputFormat.setOutputPath(job, new Path(args[0]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

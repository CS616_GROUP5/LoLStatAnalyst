package jobs;

import mapper.DummyMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import reducer.DummyReducer;

import java.net.URI;

public class LSAMain {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, args[0]);
        job.setJarByClass(LSAMain.class);
        job.setMapperClass(DummyMapper.class);
        job.setCombinerClass(DummyReducer.class);
        job.setReducerClass(DummyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.addCacheFile(new URI( "path#tag"));
        FileInputFormat.addInputPath(job, new Path("inputPth1"));
        FileInputFormat.addInputPath(job, new Path("inputPth2"));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}

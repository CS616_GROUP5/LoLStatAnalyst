package utils.beans;

import java.util.ArrayList;
import java.util.List;

public class Data {
    private List<Ward> wardFrames = new ArrayList<Ward>();

    public List<Ward> getWardFrames() {
        return wardFrames;
    }

    public void setWardFrames(List<Ward> wardFrames) {
        this.wardFrames = wardFrames;
    }

    public void addWard(Ward ward){
        wardFrames.add(ward);
    }
}

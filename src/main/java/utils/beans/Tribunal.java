package utils.beans;

import java.util.List;

public class Tribunal {
    private List<Chat> chat_log;
    private String most_common_report_reason;

    public String getMost_common_report_reason() {
        return most_common_report_reason;
    }

    public void setMost_common_report_reason(String most_common_report_reason) {
        this.most_common_report_reason = most_common_report_reason;
    }

    public List<Chat> getChat_log() {
        return chat_log;
    }

    public void setChat_log(List<Chat> chat_log) {
        this.chat_log = chat_log;
    }
}

package utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class GameParser {
    private JSONObject game;
    private HashMap<String, Boolean> playerWinning = new HashMap<String, Boolean>();
    private HashMap<String, Boolean> championsWinRate = new HashMap<String, Boolean>();
    private JSONArray players;
    private int winningTeamID;
    private JSONArray champions;
    private String region;
    private int mapID;
    private double length;
    private JSONArray minionKillEvents;

    private HashSet<Integer> buffTypes = new HashSet<Integer>();



    public GameParser(String jsonLine) {
        game = new JSONObject(jsonLine);
        winningTeamID = game.getInt("winner");
        players = game.getJSONArray("players");
        champions = game.getJSONArray("champion_ids");
        region = game.getString("region");
        mapID = game.getInt("map_id");
        playerWinning = playerWinning();
        championsWinRate = championsWinRate();
        length = game.getDouble("length");
        minionKillEvents = game.getJSONObject("data").getJSONArray("minionKillEvents");


    }

    public ArrayList<String> getPlayerList() {
        return new ArrayList<String>(playerWinning.keySet());
    }

    public boolean isWinPlayer(String name) {
        return playerWinning.get(name);
    }

    public JSONObject getPlayerByID(int id) throws JSONException {
        JSONObject player = players.getJSONObject(id % 10);
        return player;
    }

    public HashSet<Integer> getBuffTypes() {
        return buffTypes;
    }

    public JSONObject getGame() {
        return game;
    }

    public String getRegion() {
        return region;
    }

    public int getMapID() {
        return mapID;
    }

    public String getBuffInfoStr() {
        StringBuilder sb = new StringBuilder();
        double length = game.getDouble("length");
        //Get Buffer List
        JSONArray buffGainedEventList = game.getJSONObject("data").getJSONArray("buffGainedEvents");
        double d_win_100 = 0;
        double d_lose_100 = 0;
        double d_win_101 = 0;
        double d_lose_101 = 0;
        double d_win_200 = 0;
        double d_lose_200 = 0;
        double d_win = 0;
        double d_lose = 0;
        for (Object o : buffGainedEventList) {
            JSONObject event = (JSONObject) o;
            try {
                JSONObject gainedPlayer = getPlayerByID(event.getInt("unitID"));
                boolean isWin = isWinPlayer(gainedPlayer.getString("name"));
                int buffType = event.getInt("buffName");
                buffTypes.add(buffType);
                double duration = event.getDouble("duration");
                if (isWin) {
                    d_win += duration;
                    switch (buffType) {
                        case 100:
                            d_win_100 += duration;
                            break;
                        case 101:
                            d_win_101 += duration;
                            break;
                        case 200:
                            d_win_200 += duration;
                    }
                } else {
                    d_lose += duration;
                    switch (buffType) {
                        case 100:
                            d_lose_100 += duration;
                            break;
                        case 101:
                            d_lose_101 += duration;
                            break;
                        case 200:
                            d_lose_200 += duration;
                    }
                }
            } catch (JSONException je) {
                System.out.println("[JE]:PID" + event.getInt("unitID") + " PL" + getPlayerList());
                je.printStackTrace();
            }

        }
        // format: region/length/win100/win101/win200/winall/lose100/lose101/lose200/loseall
        return getRegion() + "\t" + length + "\t" + d_win_100 + "\t" + d_win_101 + "\t"
                + d_win_200 + "\t" + d_win + "\t"
                + d_lose_100 + "\t" + d_lose_101 + "\t" + d_lose_200 + "\t" + d_lose;
    }


    public HashMap<String, Boolean> playerWinning() {
        HashMap<String, Boolean> playerWinning = new HashMap<String, Boolean>();

        for (Object o : players) {
            JSONObject player = (JSONObject) o;
            String name = player.getString("name");
            boolean isWin = player.getInt("teamID") == winningTeamID;
            playerWinning.put(name, isWin);
        }
        return playerWinning;
    }

    public HashMap<String, Boolean> championsWinRate() {
        HashMap<String, Boolean> championsWinRate = new HashMap<String, Boolean>();

        for (int i = 0; i < champions.length(); i++) {
            int championID = champions.getInt(i);
            JSONObject player = players.getJSONObject(i);
            boolean is_Win = player.getInt("teamID") == winningTeamID;
            championsWinRate.put(Integer.toString(championID), is_Win);
        }
        return championsWinRate;
    }

    public ArrayList<String> getChampionList() {
        return new ArrayList<String>(championsWinRate.keySet());
    }

    public boolean isWinChampion(String champion) {
        return championsWinRate.get(champion);
    }



    public HashMap<String, Integer> MinionKillerResult(){
        HashMap<String, Integer> minionResult = new HashMap<String, Integer>();
        minionResult.put("1st_W", 0);
        minionResult.put("1st_L", 0);
        minionResult.put("2nd_W", 0);
        minionResult.put("2nd_L", 0);
        minionResult.put("3rd_W", 0);
        minionResult.put("3rd_L", 0);


        double t1 = length / 3;
        double t2 = 2 * length / 3;

        for (Object o : minionKillEvents){
            JSONObject event = (JSONObject) o;
            double time = event.getDouble("time");
            int killerID = event.getInt("killerUnitID");
            boolean win = playId_isWinner(killerID);
            if(time <= t1){
                if(win){
                    minionResult.put("1st_W", minionResult.get("1st_W") + 1);
                }
                else{
                    minionResult.put("1st_L", minionResult.get("1st_L") + 1);
                }
            }
            else if(time <= t2){
                if(win){
                    minionResult.put("2nd_W", minionResult.get("2nd_W") + 1);
                }
                else{
                    minionResult.put("2nd_L", minionResult.get("2nd_L") + 1);
                }
            }
            else {
                if(win){
                    minionResult.put("3rd_W", minionResult.get("3rd_W") + 1);
                }
                else{
                    minionResult.put("3rd_L", minionResult.get("3rd_L") + 1);
                }
            }


        }

        return minionResult;

    }



    public boolean playId_isWinner(int id){
//        JSONObject player = getPlayerByID(id);
        try{
            JSONObject player = players.getJSONObject(id);
            return player.getInt("teamID") == winningTeamID;
        } catch (JSONException e){
            System.out.println("[inPlayerID]"+players.length());
        }
        return false;
    }





}

package utils;

import com.alibaba.fastjson.JSON;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import utils.beans.Chat;
import utils.beans.Tribunal;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HateWordUtils {

    public static String getChatContent(String json){
        Tribunal tribunal = JSON.parseObject(json, Tribunal.class);
        StringBuffer content = new StringBuffer();
        for (Chat sen : tribunal.getChat_log())
            content.append(sen.getMessage()).append(".");

        return content.toString();
    }

    public static int getChatNum(String json){
        Tribunal tribunal = JSON.parseObject(json, Tribunal.class);
        return tribunal.getChat_log().size();
    }

    public static List<String> filterstopwords(String rawwords){
        List<String> words = new ArrayList<>();
        Analyzer analyzer = new StandardAnalyzer();
        TokenStream stream = null;
        try {
            stream = analyzer.tokenStream("content", new StringReader(rawwords));
            CharTermAttribute attr = stream.addAttribute(CharTermAttribute.class);
            stream.reset();
            while (stream.incrementToken())
                words.add(attr.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(stream != null){
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return words;
    }
}

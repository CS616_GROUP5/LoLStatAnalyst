package utils;

import utils.beans.LolObject;
import utils.beans.Player;
import utils.beans.Ward;

import java.util.List;


public class WardPosUtils {

    public static boolean isWinner(LolObject obj, Ward ward){
        int winner = obj.getWinner();
        int playerId = ward.getCasterUnitID();
        List<Player> playerList = obj.getPlayers();

        for (Player player : playerList) {
            if (player.getLocalID() == playerId)
                return player.getTeamID() == winner;
        }

        return false;
    }
}

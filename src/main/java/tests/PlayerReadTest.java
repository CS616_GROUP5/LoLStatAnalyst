package tests;

import utils.GameParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PlayerReadTest {
    public static void main(String[] args) {
        try {
            BufferedReader r = new BufferedReader(new FileReader("riftwalk-sample.json"));
            GameParser parser = new GameParser(r.readLine());
//            for (String name : parser.getPlayerList()) {
//                System.out.println(name + " " + parser.isWinPlayer(name));
//            }

            for(String s : parser.getChampionList()){
                System.out.println(s + " " + parser.isWinChampion(s));
            }
            System.out.println(parser.getMapID());
            System.out.println(parser.getRegion());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
